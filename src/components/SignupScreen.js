import React, {useState} from 'react'
import BaseScreen from './BaseScreen';
import {Link} from 'react-router-dom'
import { API } from "../backend";


const SignupScreen = () => {
    const [values, setValues] = useState({
        name: "",
        email: "",
        password: "",
        error: "",
        success: false
      });

      const { name, email, password, error, success } = values;

      const handleChange = name => event => {
        setValues({ ...values, error: false, [name]: event.target.value });
      };
    
 const signup = user => {
     console.log('ieeeeeeee',user)
    return fetch(`${API}/signup`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(user)
    })
      .then(response => {
        return response.json();
      })
      .catch(err => console.log(err));
  };

  const onSubmit = event => {
    event.preventDefault();
    // alert()
    setValues({ ...values, error: false });
    signup({ name, email, password })
      .then(data => {
        if (data.error) {
          setValues({ ...values, error: data.error, success: false });
        } else {
          setValues({
            ...values,
            name: "",
            email: "",
            password: "",
            error: "",
            success: true
          });
        }
      })
      .catch(console.log("Error in signup"));
  };


    const signupForm = () => {
        return (
            <div className="container mt-1">
                <div className='row justify-content-md-center'>
                    <div className='col-md-6'>
                    <h5 className='text-center'>Create an account</h5>
                        <div className='card mt-3'>
                            <div className='card-body'>
                                <form className="">
                                    <div className="form-group ">
                                        <label>Name</label>
                                        <input
                                            type="text"
                                            name='name'
                                            onChange={handleChange("name")}
                                            value={name}
                                            className="form-control"
                                            placeholder="Enter Name"/>
                                    </div>
                                    <div className="form-group ">
                                        <label>Email</label>
                                        <input
                                            type="email"
                                            onChange={handleChange("email")}
                                            value={email}
                                            name='email'
                                            className="form-control"
                                            placeholder="Enter username"/>
                                    </div>
                                    <div className="form-group">
                                        <label>Password</label>
                                        <input
                                            type="password"
                                            name='password'
                                            onChange={handleChange("password")}
                                            value={password}
                                            className="form-control"
                                            placeholder="Enter password"/>
                                    </div>
                                    <div className='text-center mt-5 mb-4'>
                                        <button onClick={onSubmit} class="btn btn btn-block btn-outline-info ml-center">SignUp</button>

                                    </div>
                                    <p className='font-weight-bold'>Already have an account</p>
                                    <div className='text-center mt-4 mb-3'>
                                        <Link to='/signin'>
                                            <button className="btn btn-block  ml-center">SignIn</button>
                                        </Link>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    const successMessage = () => {
        return (
          <div className="row">
            <div className="col-md-6 offset-sm-3 text-left">
              <div
                className="alert alert-success"
                style={{ display: success ? "" : "none" }}
              >
                New account was created successfully. Please
                <Link to="/signin">Login Here</Link>
              </div>
            </div>
          </div>
        );
      };

      const errorMessage = () => {
        return (
          <div className="row">
            <div className="col-md-6 offset-sm-3 text-left">
              <div
                className="alert alert-danger"
                style={{ display: error ? "" : "none" }}
              >
                {error}
              </div>
            </div>
          </div>
        );
      };
    return (<BaseScreen>
    {successMessage()}
      {errorMessage()}
        {signupForm()}
    </BaseScreen>)
}

export default SignupScreen
