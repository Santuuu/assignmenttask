import React, {useState, useEffect} from "react";
import {API} from "../backend";
import Card from "./CardScreen";
import BaseScreen from "./BaseScreen";

export default function Home() {
    const [products, setProducts] = useState([]);
    const [error, setError] = useState(false);

    const getProducts = () => {
        return fetch(`${API}/products`, {method: "GET"})
            .then(response => {
                return response.json();
            })
            .catch(err => console.log(err));
    };

    const loadAllProduct = () => {
        getProducts().then(data => {
            if (data) {
                setProducts(data);
            } 
        });
    };

    useEffect(() => {
        loadAllProduct();
    }, []);

    return (
        <BaseScreen>
            <div className="row text-center">
                {
                    products.map((product, index) => {
                        return (
                            <div key={index} className='col-md-4'>
                                <Card product={product}/>
                            </div>
                        );
                    })
                }
            </div>
        </BaseScreen>
    );
}
