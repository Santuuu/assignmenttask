import React from "react";

const Card = ({product}) => {

    console.log('wjk',product)
const image = 'https://images.pexels.com/photos/3561339/pexels-photo-3561339.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940'
    return (
        <div className="card mb-4">

            <img class="card-header lead card-img-top" src={image} alt="Card image cap"/>
            <div className="card-body">
                <p><span className='font-weight-bold'>Product :</span> {product.name}</p>
                <p>
                <span className='font-weight-bold'>Discription :</span> {product.description}
                </p>
                <p><span className='font-weight-bold'>Price :</span>$ {product.price}</p>
                <div className="row justify-content-center">
                    <div className="col-4 btn btn-info m-1">Info</div>
                    <div className="col-4 btn btn-warning m-1">Cart</div>
                </div>
            </div>
        </div>
    );
};

export default Card;
