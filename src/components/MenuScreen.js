import React from 'react'
import '../App.css'
import '../index.css'
import {Link, withRouter} from 'react-router-dom'

const MenuScreen = () => {
    return (
        <nav className="navbar navbar-expand-lg navbar-light">
        <Link className="navbar-brand1" to='/'>Project_Logo</Link>
            <button
                className="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarNav"
                aria-controls="navbarNav"
                aria-expanded="false"
                aria-label="Toggle navigation">
                {/* <span className="navbar-toggler-icon"></span> */}
                <i class="fa fa-bars"></i>

            </button>
            <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav ml-auto">
                    {/* <li className="nav-item">
                        <Link className="nav-link" to='/'>Home
                        </Link>
                    </li> */}
                    <li className="nav-item">
                    <Link className="nav-link" to='/signup'>SignUp</Link>
                    </li>
                    <li className="nav-item">
                    <Link className="nav-link" to='/signin'>SignIn</Link>
                    </li>
                </ul>
            </div>
        </nav>
    )
}

export default withRouter(MenuScreen);
