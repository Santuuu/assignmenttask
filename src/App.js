import React from 'react';
import './App.css';
import './index.css'
import {BrowserRouter, Switch, Route} from "react-router-dom";
import HomeScreen from './components/HomeScreen';
import SignupScreen from './components/SignupScreen';
import SigninScreen from './components/SigninScreen';



function App() {
    return (
        <div className="App">
            <BrowserRouter>
                <Switch>
                    <Route path="/" exact component={HomeScreen}/>
                    <Route path="/signup" exact component={SignupScreen}/>
                    <Route path="/signin" exact component={SigninScreen}/>
                    </Switch>
            </BrowserRouter>
        </div>
    );
}

export default App;
