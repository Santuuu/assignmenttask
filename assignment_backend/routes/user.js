const express = require("express");
const router = express.Router();
const { check, validationResult } = require("express-validator");

const { signup,signin } = require("../controllers/user");

//Signup route
router.post(
    "/signup",
    [
      check("name", "name should be at least 3 char").isLength({ min: 3 }),
      check("email", "email is required").isEmail(),
      check("password", "password should be at least 3 char").isLength({ min: 3 })
    ],
    signup
  );

  //signin route
  router.post(
    "/signin",
    [
      check("email", "email is required").isEmail(),
      check("password", "password field is required").isLength({ min: 1 })
    ],
    signin
  );


module.exports = router;
