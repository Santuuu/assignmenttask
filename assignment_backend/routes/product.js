const express = require("express");
const router = express.Router();

const {createProduct,getProducts,photo} = require("../controllers/product");

//create route
router.post("/product/create", createProduct);

// read routes
router.get("/products", getProducts);
router.get("/product/photo/", photo);


module.exports = router;
