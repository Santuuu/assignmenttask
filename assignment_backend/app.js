const express = require('express')
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const cors = require("cors");


const app = express()

//DB Connection
mongoose
    .connect('mongodb://localhost/tshirt,', {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true
    })
    .then(() => {
        console.log("DB CONNECTED...");
    });

//My routes
const userRoutes = require("./routes/user");
const productRoutes = require('./routes/product')

//Middlewares
app.use(bodyParser.json());
app.use(cors());


app.use("/api", userRoutes);
app.use("/api", productRoutes);

//PORT
const port = 8000;

//Starting a server
app.listen(port, () => {
    console.log(`App is running at ${port}`);
});
